# 电影院购票小程序<br>

## 项目简介

电影院购票小程序是一套仿照市面上已有的购票小程序开发的一套入门级微信小程序。<br>
本系统分为小程序端和管理端，小程序端主要是为用户提供电影查询，选座，下单，购票的功能。管理端主要是为管理员提供管理电影等其他功能。<br>
电影院购票小程序比较贴合学生平时的生活需求，题目经典不失新颖，且界面美观大方，非常适合学习使用。<br>
视频介绍：<a href="https://www.bilibili.com/video/BV18X4y1L79n/?spm_id_from=333.999.0.0&vd_source=e6eb98771fefe4bc68298183ff572b90" target="_blank">点击查看视频介绍</a>

## 功能介绍

<img src="./images/gn.png" width="600" height="450" />


## 系统图片

#### 1.首页
<img src="./images/1.jpg" width="300" height="534" /><br>
#### 2.电影分类
<img src="./images/2.jpg" width="300" height="534" /><br>
#### 3.查看电影详情
<img src="./images/3.jpg" width="300" height="534" /><br>
#### 4.选座购票
<img src="./images/4.png" width="300" height="534" /><br>
#### 5.查看订单
<img src="./images/5.png" width="300" height="534" /><br>
#### 6.我的
<img src="./images/6.png" width="300" height="534" /><br>
#### 7.管理端登录页
<img src="./images/7.png" width="600" height="300" /><br>
#### 8.电影场次管理
<img src="./images/8.png" width="600" height="300" /><br>

